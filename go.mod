module github.com/leizongmin/jssh

go 1.15

require (
	github.com/creack/pty v1.1.11
	github.com/gookit/color v1.3.6
	github.com/json-iterator/go v1.1.10
	github.com/leizongmin/go v1.9.5
	github.com/lithdew/quickjs v0.0.0-20200714182134-aaa42285c9d2
	github.com/peterh/liner v1.2.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf
)
